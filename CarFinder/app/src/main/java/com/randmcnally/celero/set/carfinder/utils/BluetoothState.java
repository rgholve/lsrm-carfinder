package com.randmcnally.celero.set.carfinder.utils;

/**
 * Created by mjunghare on 27/1/16.
 */
public enum BluetoothState {

    TURNON(0), TURNEDEOFF(1), CONNECTED(11), CONNECTING(12), DISCONNECTED(13);

    private int value;

    private BluetoothState(int value) {
        this.value = value;
    }
}

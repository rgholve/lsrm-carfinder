package com.randmcnally.celero.set.carfinder.Network;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.leapscale.btnetwork.BTConstants;
import com.example.leapscale.btnetwork.BluetoothNetwork;
import com.randmcnally.celero.set.carfinder.Interface.IServiceMessages;
import com.randmcnally.celero.set.carfinder.R;
import com.randmcnally.celero.set.carfinder.utils.DataModel;
import com.randmcnally.celero.set.carfinder.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

/**
 * Created by asilveanu on 8/12/15.
 */
public class BackgroundService extends Service {

    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    //    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static String previousPhoneState = "";
    private final String TAG = getClass().getName();
    StringBuilder buildReceivedData = new StringBuilder();
    private int count = 0;


    private boolean isRMTablet = false;

    private WiFiNetwork wifiNetwork = null;

    private BluetoothNetwork btNetwork = null;

    private SharedPreferences sharedPreferences = null;

    private BluetoothAdapter btAdapter = null;



//    private Messenger messageHandler;

    private BluetoothReceiver btReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate");



        // Get inbuilt Bluetooth adapter
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (btAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
        }

        sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);


        wifiNetwork = new WiFiNetwork(this);
        btNetwork = new BluetoothNetwork(this);

        registerBluetoothReceiver();
    }

    IServiceMessages serviceCallback;
    public void registerCallback(IServiceMessages callback) {
        serviceCallback = callback;
    }

    private void registerBluetoothReceiver() {
        btReceiver = new BluetoothReceiver();
        IntentFilter intentFilter = new IntentFilter(BTConstants.BT_ACTION);
        registerReceiver(btReceiver, intentFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        return START_STICKY;
    }




    public class LocalBinder extends Binder {
        public BackgroundService getInstance() {
            return BackgroundService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
//        messageHandler = (Messenger) intent.getExtras().get(Constants.MESSENGER);
        return new LocalBinder();
    }

    public void startBtNetwork() {
        if(btNetwork != null &&  btAdapter.isEnabled()) {
            btNetwork.start();
        }
    }

    public void unRegisterCallback() {
        serviceCallback = null;
    }
    public void connect(String address, boolean isSecure) {
        if(btNetwork != null) {
            // Get the BluetoothDevice object
            BluetoothDevice device = btAdapter.getRemoteDevice(address);
            // Attempt to connect to the device
            btNetwork.connect(device, isSecure);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    public void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (BluetoothNetwork.mState != BTConstants.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            if(btNetwork != null)
                btNetwork.write(send);
        }
    }

    public void sendImageMessage(String bitmapString){
        if (BluetoothNetwork.mState != BTConstants.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (bitmapString.length() > 0) {
            bitmapString = "##" + bitmapString + "##";                 //starts and ends with ##
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = bitmapString.getBytes();
            if(btNetwork != null)
                btNetwork.writeImageBytes(send);
        }
    }

 /*
    public void sendImageMessage(String bitmapString) {
        if (BluetoothNetwork.mState != BTConstants.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
          bitmapString = "##" + bitmapString + "##";
        Log.e(TAG, "BitmapString=" + bitmapString);
        // Check that there's actually something to send
        int i=0;
        if (bitmapString.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = bitmapString.getBytes();
            if(btNetwork != null) {
                ByteArrayInputStream bis = new ByteArrayInputStream(send);
                byte[] chunk = new byte[1024 * 3];
                while( bis.read(chunk, 0, chunk.length) != -1 ) {


                    btNetwork.write(chunk);
                    chunk = new byte[1024 * 3];
                    Log.e(TAG,""+ ++i +" Chunk="+chunk.toString());
                }
            }
        }
    }

*/



    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterBluetoothReceiver();
        Log.d(TAG, "onDestroy");


    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);

        Log.d(TAG, "onRebind");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        boolean status = super.onUnbind(intent);
//        unRegisterBluetoothReceiver();
        return status;
    }













    class BluetoothReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int action = intent.getExtras().getInt(BTConstants.KEY_ACTION);
            switch (action) {
                case BTConstants.MSG_RECEIVED:
                    String receivedData = intent.getExtras().getString(BTConstants.KEY_MSG);
                    if(!Utils.isTablet) {
                        parseAndSaveData(receivedData);
                    }
                    Log.e(TAG, receivedData);
                    Toast.makeText(getApplicationContext(), receivedData, Toast.LENGTH_SHORT).show();
                    break;
                case BTConstants.STATE_CONNECTED:

//                    Toast.makeText(getApplicationContext(), "STATE CONNECTED", Toast.LENGTH_SHORT).show();
                    String deviceName = intent.getExtras().getString(BTConstants.KEY_MSG);
//                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.title_connected_to, deviceName), Toast.LENGTH_SHORT).show();
//                    Message message = Message.obtain();
//                    message.obj = deviceName;
//                    try {
//                        messageHandler.send(message);
//                    } catch (RemoteException e) {
//                        e.printStackTrace();
//                    }
                    if(serviceCallback != null)
                    serviceCallback.handleMessages(BTConstants.STATE_CONNECTED, deviceName);
                    break;
                case BTConstants.STATE_CONNECTING:
                    break;
                case BTConstants.STATE_LISTEN:
                    break;
                case BTConstants.STATE_NONE:
                    break;
                case BTConstants.STATE_DISCONNECTED:
                    String errorMessage = intent.getExtras().getString(BTConstants.KEY_MSG);
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    if(serviceCallback != null)
                    serviceCallback.handleMessages(BTConstants.STATE_DISCONNECTED, errorMessage);
                    break;
                default:
                    break;
            }
        }
    }

    private void parseAndSaveData(String receivingData) {
        DataModel dataModel = new DataModel();
        try {
            JSONObject mainObj = new JSONObject(receivingData);
            if(mainObj != null) {
                JSONObject locObj = mainObj.optJSONObject("Location");
                if(locObj != null) {
                    dataModel.setLatitude(locObj.optString("Latitude", ""));
                    dataModel.setLongitude(locObj.optString("Longitude", ""));
                }
                dataModel.setStatus(mainObj.optString("Status", ""));
                dataModel.setDeviceId(mainObj.optString("Deviceid", ""));
                dataModel.setDate(mainObj.optString("Date", ""));
                dataModel.setTimestamp(mainObj.optString("Timestamp", ""));
                dataModel.setPhoto(mainObj.optString("Photo", ""));

                Utils.setSharedPrefs(getApplicationContext(), dataModel);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void unRegisterBluetoothReceiver() {
        if(btReceiver != null)
            unregisterReceiver(btReceiver);
    }

    public void unpairDevice() {
        if(btNetwork != null) {
            btNetwork.unpairDevice();
        }
    }

    class IncomingMessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }
}

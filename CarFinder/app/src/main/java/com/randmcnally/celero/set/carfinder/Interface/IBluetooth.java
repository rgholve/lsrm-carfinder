package com.randmcnally.celero.set.carfinder.Interface;

/**
 * Created by mjunghare on 27/1/16.
 */
public interface IBluetooth {
    public void onBluetoothTurnOn();
    public void onBluetoothTurnOff();
    public void onBluetoothConnected();
    public void onBluetoothDisConnected();
    public void onBluetoothReceiveData(String data);
    public void onBluetoothSendData();
}

package com.randmcnally.celero.set.carfinder.uiscreens;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.leapscale.btnetwork.BTConstants;
import com.randmcnally.celero.set.carfinder.Interface.OnButtonClickListener;
import com.randmcnally.celero.set.carfinder.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParkCarFragment extends Fragment {

    private OnButtonClickListener mClickListener;
    private Button buttonLocation;
    private ImageView imgMap;
    private boolean makeVisible;
    public static ParkCarFragment newInstance(OnButtonClickListener mClickListener, boolean makeVisible) {
        ParkCarFragment parkCarFragment = new ParkCarFragment();
        parkCarFragment.mClickListener = mClickListener;
        parkCarFragment.makeVisible = makeVisible;
        return  parkCarFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_park_car, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgMap = (ImageView) view.findViewById(R.id.img_map);
        buttonLocation = (Button) view.findViewById(R.id.button_send_location);

        if (makeVisible){
            imgMap.setBackgroundResource(R.drawable.map_raw);
            buttonLocation.setVisibility(View.VISIBLE);
            buttonLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mClickListener != null)
                        mClickListener.onClickParkCarButton();

                }
            });
        }
        else if(!makeVisible){
            imgMap.setBackgroundResource(R.drawable.map_raw2);
            buttonLocation.setVisibility(View.INVISIBLE);

        }
    }
}

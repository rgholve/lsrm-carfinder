package com.randmcnally.celero.set.carfinder.uiscreens;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.leapscale.btnetwork.BTConstants;
import com.example.leapscale.btnetwork.BluetoothNetwork;
import com.example.leapscale.btnetwork.DeviceListActivity;
import com.google.android.gms.maps.model.LatLng;
import com.randmcnally.celero.set.carfinder.Interface.IServiceMessages;
import com.randmcnally.celero.set.carfinder.Interface.OnButtonClickListener;
import com.randmcnally.celero.set.carfinder.Network.BackgroundService;
import com.randmcnally.celero.set.carfinder.R;
import com.randmcnally.celero.set.carfinder.utils.Constants;
import com.randmcnally.celero.set.carfinder.utils.GPSTracker;
import com.randmcnally.celero.set.carfinder.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseActivity extends AppCompatActivity implements OnButtonClickListener, IServiceMessages{
    public BluetoothAdapter mBluetoothAdapter;
    final String TAG = "BaseActivity";

    BackgroundService mService;
    boolean mServiceConnected = false;
    public static boolean mDeviceConnected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Utils.isTablet = false;
        loadHomeFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
      Log.i(TAG, "onStart");
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
        } else {
            startBackgroundService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.

        // Only if the state is STATE_NONE, do we know that we haven't started already
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
        if (BluetoothNetwork.mState == BTConstants.STATE_NONE) {
            // Start the Bluetooth chat services
            if(mService != null && mServiceConnected) {
                mService.startBtNetwork();
            }
        } else if(BluetoothNetwork.mState == BTConstants.STATE_CONNECTED) {
            if(fragment != null && fragment instanceof HomeFragment){
                ((HomeFragment) fragment).updatePairButtonText(getString(R.string.unpair_btn_txt));
                }
            } else if(fragment != null && fragment instanceof HomeFragment) {
            ((HomeFragment) fragment).updatePairButtonText(getString(R.string.pair_btn_txt));
            }




    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
        if(mServiceConnected) {
            Log.i(TAG, "onStop" +mServiceConnected);
            unbindService(sConn);
            mServiceConnected = false;
            mService.unRegisterCallback();
        }
    }

    public void startBackgroundService() {
        Log.i(TAG, "startBackgroundService");
        Intent intentService = new Intent(this, BackgroundService.class);
//        if(mService == null) {
            startService(intentService);
            bindService(intentService, sConn, Context.BIND_AUTO_CREATE);
//        }
//        else {
//            bindService(intentService, sConn, Context.BIND_AUTO_CREATE);
//        }
    }

    public void loadHomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HomeFragment homeFragment = HomeFragment.newInstance(this);
        fragmentTransaction.replace(R.id.containerView, homeFragment, HomeFragment.class.getName());
        fragmentTransaction.commit();
    }


    @Override
    public void onClickPairButton() {
//        Toast.makeText(getApplicationContext(), "OnClickPair", Toast.LENGTH_SHORT).show();

           if (!mBluetoothAdapter.isEnabled()) {
               Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
               startActivityForResult(enableIntent, Constants.REQUEST_ENABLE_BT);
               // Otherwise, setup the chat session
           } else {
               showPairDialog();
           }

    }

    @Override
    public void onClickUnpairButton() {
        if(mService != null)
         mService.unpairDevice();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
        if(currentFragment.getTag().equalsIgnoreCase("HomeFragment")) {
            Toast.makeText(getApplicationContext(), "HomeFragment", Toast.LENGTH_SHORT).show();
        }*/

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
        if(fragment != null && fragment instanceof HomeFragment){
            if(mDeviceConnected)
                ((HomeFragment) fragment).updatePairButtonText(getString(R.string.unpair_btn_txt));
            else
                ((HomeFragment) fragment).updatePairButtonText(getString(R.string.pair_btn_txt));
        }
    }

    @Override
    public void onClickParkCarButton() {
        sendLocation("Park");
    }

    static boolean isImageThreadInUse = false;
    private void sendLocation(String status) {

        if (mService != null) {
            if (BluetoothNetwork.mState == BTConstants.STATE_CONNECTED) {

                if(!isImageThreadInUse) {
                    String msg = getJsonData(status);
                    sendMessage(msg);

                    isImageThreadInUse = true;
                    Thread imageThread =   new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.street);
                                sendImageMessage(Utils.getStringFromBitmap(bmp));
                                bmp = null;
                                isImageThreadInUse = false;
                            }
                        });
                    imageThread.start();

                }
                else{
                    Toast.makeText(getApplicationContext(),"Data Transfer In Progress...",Toast.LENGTH_SHORT).show();
                }

            }else {
                Toast.makeText(getApplicationContext(), "Not Conneted with Device", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendImageMessage(String bitmapString) {
        if(mService != null && mServiceConnected) {
            mService.sendImageMessage(bitmapString);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        if(mService != null && mServiceConnected) {
            mService.sendMessage(message);
        }
    }

    private String getJsonData(String status) {
        LatLng latLng = getCurrentLocation();

        String latitude = String.format("%s", latLng.latitude);
        String longitude = String.format("%s", latLng.longitude);
        String date = Utils.getUTCdatetimeAsString();
        Log.i("Current Date ", date);

     //   BitmapDrawable imageBitmapDrawable = (BitmapDrawable) (getDrawable(R.drawable.street));
     //   Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.street);
        String jsonString = "";
        try {
            JSONObject mainObj = new JSONObject();
            mainObj.put("Status", status);

            JSONObject locObj = new JSONObject();
            locObj.put("Latitude", latitude);
            locObj.put("Longitude", longitude);
            mainObj.put("Location", locObj);
            mainObj.put("Deviceid", "");
            mainObj.put("Date", date);
            mainObj.put("Timestamp", "");
            mainObj.put("Photo", "");
         /*   String encodedImage = Utils.getStringFromBitmap(bmp);
            mainObj.put("Photo", "\""+encodedImage+"\"");*/

            jsonString = mainObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

   //     Toast.makeText(this, "Data = "+jsonString, Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Data to be send : "+jsonString);
        return jsonString;
    }

    public LatLng getCurrentLocation() {
        GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
        LatLng userLocation =  new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        return userLocation;
    }



    ServiceConnection sConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "onServiceConnected");
            mService = ((BackgroundService.LocalBinder)service).getInstance();
            mServiceConnected = true;
            mService.registerCallback(BaseActivity.this);

            if (BluetoothNetwork.mState == BTConstants.STATE_NONE) {
                // Start the Bluetooth chat services
                if(mService != null) {
                    Log.i(TAG, "onServiceConnected" + mServiceConnected);
                    mService.startBtNetwork();
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected");
            mService = null;
            mServiceConnected = false;
        }
    };

    public void showPairDialog() {
        Intent deviceListIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(deviceListIntent, Constants.REQUEST_CONNECT_DEVICE_INSECURE);
    }

    /**
     * Establish connection with other device
     * @param address address if the device to be connected
     */
    private void connectDevice(String address) {
        if(mService != null) {
            mService.connect(address, true);
        }
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
      /*      case Constants.REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;*/
            case Constants.REQUEST_CONNECT_DEVICE_INSECURE:
                Log.i(TAG, "REQUEST_CONNECT_DEVICE_INSECURE");
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
             //       showAlertDialog();
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    connectDevice(address);
                }
                break;
            case Constants.REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if(mService != null && mServiceConnected) {
                        mService.startBtNetwork();
                    }
                    showPairDialog();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
//                    finish();
                }
        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
        alertDialog.setTitle(getString(R.string.remember_device_title));
        alertDialog.setMessage(getString(R.string.remember_device_text1) + "\n" + getString(R.string.remember_device_text2));
        alertDialog.setIcon(android.R.drawable.ic_menu_save);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
       alertDialog.show();
    }

    @Override
    public void handleMessages(int state, String Message) {
        switch (state) {
            case BTConstants.STATE_CONNECTED:
                Log.i(TAG, "STATE_CONNECTED");
                mDeviceConnected = true;
                String mConnectedDeviceName = Message;
                Toast.makeText(getApplicationContext(), getString(R.string.title_connected_to, mConnectedDeviceName), Toast.LENGTH_SHORT).show();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
                if(fragment != null && fragment instanceof HomeFragment){
                    ((HomeFragment) fragment).updatePairButtonText(getString(R.string.unpair_btn_txt));
                }
                break;
            case BTConstants.STATE_CONNECTING:
//                            setStatus(R.string.title_connecting);
                break;
            case BTConstants.STATE_LISTEN:
            case BTConstants.STATE_NONE:
//                      Toast.makeText(getApplicationContext(), getString(R.string.title_not_connected, mConnectedDeviceName), Toast.LENGTH_SHORT).show();
                break;

            case BTConstants.STATE_DISCONNECTED:
                mDeviceConnected = false;
                      //Toast.makeText(getApplicationContext(), getString(R.string.title_not_connected, mConnectedDeviceName), Toast.LENGTH_SHORT).show();
                fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
                if(fragment != null && fragment instanceof HomeFragment){
                    ((HomeFragment) fragment).updatePairButtonText(getString(R.string.pair_btn_txt));
                }
                break;

//                case Constants.MESSAGE_WRITE:
//                    byte[] writeBuf = (byte[]) msg.obj;
//                    // construct a string from the buffer
//                    String writeMessage = new String(writeBuf);
//                    mConversationArrayAdapter.add("Me:  " + writeMessage);
//                    break;
            case Constants.MESSAGE_READ:
//                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
//                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(BaseActivity.this, "Msg from "+mConnectedDeviceName+": "+readMessage, Toast.LENGTH_SHORT).show();
//                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                break;

        }
    }
}

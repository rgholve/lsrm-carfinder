package com.randmcnally.celero.set.carfinder.Network;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.randmcnally.celero.set.carfinder.R;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by asilveanu on 7/15/15.
 */
public class WiFiNetwork {

    public static final int CONNECTED = 1;

    //    private static WiFiNetwork instance = null;
    public static final int LISTENING = 2;
    public static final int DISCONNECTED = 0;
    private static String ipAddress;

//    private OnReceivedMessage onReceivedMessage;

    //    private OnConnectionStatus onConnectionStatus;
    private static int port;
    private final String TAG = getClass().getName();
    private ServerSocket serverSocket;
    private Socket socket = null;
    private InputStream in = null;
    private OutputStream out = null;
    private String messageToSend;
    private Context context;

//    protected WiFiNetwork() {
//
//    }

    public WiFiNetwork(Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        String wifi_ip_address = sharedPreferences.getString("wifi_ip_address", "");
        String strWifiPort = sharedPreferences.getString("wifi_port", "");
        int wifi_port = 0;
        if(!strWifiPort.isEmpty())
            wifi_port = Integer.valueOf(strWifiPort);
        Log.d(TAG, "Null ipAddress Referance and port is 0");
        this.ipAddress = wifi_ip_address;
        this.port = wifi_port;
    }

//    public static WiFiNetwork getInstance() {
//        if(instance == null) {
//            instance = new WiFiNetwork();
//        }
//
//        return instance;
//    }

//    public void setOnConnectionStatus(OnConnectionStatus onConnectionStatus)
//    {
//        this.onConnectionStatus = onConnectionStatus;
//    }

    public static String currentIPAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        String ipString = String.format(
                "%d.%d.%d.%d",
                (ip & 0xff),
                (ip >> 8 & 0xff),
                (ip >> 16 & 0xff),
                (ip >> 24 & 0xff));

        return ipString;
    }

    //    public void startClient(String ipAddress, int port, OnReceivedMessage onReceiveMessage)
    public void startClient(String ipAddress, int port)
    {
//        this.onReceivedMessage = onReceiveMessage;
        this.ipAddress = ipAddress;
        this.port = port;

        new Thread( new ClientReceiverRunnable() ).start();
    }

    public void stopClient()
    {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

//        if(onConnectionStatus!=null) onConnectionStatus.setNetworkStatus( DISCONNECTED );

        Intent wifiNetworkStatus = new Intent();
        wifiNetworkStatus.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
        wifiNetworkStatus.putExtra("status", "Disconnected");

        context.sendBroadcast(wifiNetworkStatus);
    }

    //    public void startServer(int port, OnReceivedMessage callback)
    public void startServer(int port)
    {
//        this.onReceivedMessage = callback;
        this.port = port;

        new Thread(new ServerReceiverRunnable()).start();
    }

    public void stopServer()
    {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        if(onConnectionStatus!=null) onConnectionStatus.setNetworkStatus( DISCONNECTED );
        Intent wifiNetworkStatus = new Intent();
        wifiNetworkStatus.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
        wifiNetworkStatus.putExtra("status", "Disconnected");

        context.sendBroadcast(wifiNetworkStatus);
    }

    public void sendToClient(String message)
    {
        messageToSend = message;

        Log.d(TAG, "sendToClient ipaddress: " + ipAddress + "  port: " + port);

        new Thread(new ServerSenderRunnable()).start();
    }

    public void sendToServer(String message)
    {
        messageToSend = message + "##";

        Log.d(TAG, "sendToServer ipaddress: " + ipAddress + "  port: " + port);
        if (ipAddress != null && port != 0)
            new Thread(new ClientSenderRunnable()).start();
        else {
            Log.d(TAG, "Null ipAddress Referance and port is 0");
        }
    }

    public boolean isConnected() {
        if (socket == null) {
            return false;
        } else {
            return socket.isConnected();
        }
    }

    private void ipConfigurationIssue() {
        Log.e(TAG, "Check the IP address on the tablet matches the smartphone");

        Intent checkIPErrorIntent = new Intent("com.randmcnally.handsfreecalling.checkiperror");
        context.sendBroadcast(checkIPErrorIntent);
    }

    private class ServerSenderRunnable implements Runnable {

        @Override
        public void run() {

            DataOutputStream dataOutputStream = null;

            try {
                Log.d(TAG, "Out: " + ((out == null) ? "is NULL" : "is NOT null"));
                dataOutputStream = new DataOutputStream(out);
                Log.d(TAG, "dataOutputStream: " + ((dataOutputStream == null) ? "is NULL" : "is NOT null"));

                Log.d(TAG, "Sent to client: " + messageToSend);

                if (messageToSend != null && dataOutputStream != null) {

                    StreamHelper.writeString(dataOutputStream, messageToSend);
//                    dataOutputStream.writeUTF(messageToSend);
//                    dataOutputStream.flush();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ServerReceiverRunnable implements Runnable {

        @Override
        public void run() {

            DataInputStream dataInputStream = null;

            Intent wifiNetworkStatusIntent = new Intent();

            try {
                Log.d(TAG, "PORT :" + port);
                serverSocket = new ServerSocket(port);
//                if (onConnectionStatus != null)
//                    onConnectionStatus.setNetworkStatus( LISTENING );
                wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                wifiNetworkStatusIntent.putExtra("status", "Listening");

                context.sendBroadcast(wifiNetworkStatusIntent);

                // This method is thread blocking until connected
                socket = serverSocket.accept();

                if (socket.isConnected()) {
//                    if (onConnectionStatus != null)
//                        onConnectionStatus.setNetworkStatus( CONNECTED);

                    wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                    wifiNetworkStatusIntent.putExtra("status", "Connected");

                    context.sendBroadcast(wifiNetworkStatusIntent);

                    Log.d(TAG, "WiFi Connected");
                }

                try {
                    Log.d(TAG, "Get: socket.getInputStream()");
                    in = socket.getInputStream();
                    Log.d(TAG, "Obtained: socket.getInputStream()");

                    Log.d(TAG, "Get: socket.getOutputStream()");
                    out = socket.getOutputStream();
                    Log.d(TAG, "Obtained: socket.getOutputStream()");


                } catch (IOException e) {
                }

                try {
                    while (true) {
                        dataInputStream = new DataInputStream(in);

//                        final String receivedString = dataInputStream.readUTF();
                        final String receivedString = StreamHelper.readString(dataInputStream);

                        Log.d(TAG, "Server received: " + receivedString);

//                        if(onReceivedMessage != null)onReceivedMessage.callback( receivedString );

                        Intent wifiNetworkReceivedDataIntent = new Intent();
                        wifiNetworkReceivedDataIntent.setAction("com.randmcnally.handsfreecalling.handlerequest");
                        wifiNetworkReceivedDataIntent.putExtra("data", receivedString);

                        context.sendBroadcast(wifiNetworkReceivedDataIntent);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

//                    if (onConnectionStatus != null)
//                        onConnectionStatus.setNetworkStatus( DISCONNECTED );

                    wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                    wifiNetworkStatusIntent.putExtra("status", "Disconnected");

                    context.sendBroadcast(wifiNetworkStatusIntent);

                    Log.d(TAG, "WiFi Disconnected");

                    if (socket != null) socket.close();
                    if (serverSocket != null) serverSocket.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class ClientSenderRunnable implements Runnable {
        @Override
        public void run() {

            DataOutputStream dataOutputStream = null;

            try {
                dataOutputStream = new DataOutputStream(out);

                if (messageToSend != null && dataOutputStream != null) {
                    StreamHelper.writeString(dataOutputStream, messageToSend + "##");
//                    dataOutputStream.writeUTF(messageToSend+"##");
//                    dataOutputStream.flush();

                    Log.d(TAG, "Wrote to server: " + messageToSend);
                } else {
                    Log.d(TAG, "Did not write to server");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private class ClientReceiverRunnable implements Runnable {
        @Override
        public void run() {

            DataInputStream dataInputStream = null;

            Intent wifiNetworkStatusIntent = new Intent();

            try {
                socket = new Socket(ipAddress, port);

                if (socket.isConnected()) {
//                    if(onConnectionStatus!=null) onConnectionStatus.setNetworkStatus( CONNECTED );

                    wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                    wifiNetworkStatusIntent.putExtra("status", "Connected");

                    context.sendBroadcast(wifiNetworkStatusIntent);

                    Log.d(TAG, "WiFi Connected");
                } else {
//                    if(onConnectionStatus!=null) onConnectionStatus.setNetworkStatus( DISCONNECTED );

                    wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                    wifiNetworkStatusIntent.putExtra("status", "Disconnected");

                    Log.d(TAG, "WiFi Disconnected");
                }

                try {
                    Log.d(TAG, "Get: socket.getInputStream()");
                    in = socket.getInputStream();
                    Log.d(TAG, "Obtained: socket.getInputStream()");

                    Log.d(TAG, "Get: socket.getOutputStream()");
                    out = socket.getOutputStream();
                    Log.d(TAG, "Obtained: socket.getOutputStream()");
                } catch (IOException e) {
                }

                Log.d(TAG, "Client receiver ready");

                while (true) {
                    dataInputStream = new DataInputStream(in);

//                    final String receivedText = dataInputStream.readUTF();

                    final String receivedText = StreamHelper.readString(dataInputStream);


//                    if(onReceivedMessage != null)onReceivedMessage.callback(receivedText);

                    Log.d(TAG, "Client Receiver received: " + receivedText);

                    Intent wifiNetworkReceivedDataIntent = new Intent();
                    //wifiNetworkReceivedDataIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.receiveddata");
                    wifiNetworkReceivedDataIntent.setAction("com.randmcnally.handsfreecalling.handlerequest");

                    String actionRecentcallString = "{\"action\":\"responseRecentCalls\"}##";
                    String actionContactstring = "{\"action\":\"responseContacts\"}##";     //responseContacts
                    SharedPreferences spf = context.getSharedPreferences("HFCrecentCalls", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edt = spf.edit();
                    if (receivedText.contains("responseRecentCalls")) {
                        edt.putString("data", receivedText);
                        wifiNetworkReceivedDataIntent.putExtra("data", actionRecentcallString);
                        edt.commit();
                        context.sendBroadcast(wifiNetworkReceivedDataIntent);
                    } else if (receivedText.contains("responseContacts")) {
                        edt.putString("contacts", receivedText);
                        wifiNetworkReceivedDataIntent.putExtra("data", actionContactstring);
                        edt.commit();
                        context.sendBroadcast(wifiNetworkReceivedDataIntent);
                    } else {
                        wifiNetworkReceivedDataIntent.putExtra("data", receivedText);
                        context.sendBroadcast(wifiNetworkReceivedDataIntent);
                    }
                }
            } catch (ConnectException ce) {
                ipConfigurationIssue();
            } catch (UnknownHostException ce) {
                ipConfigurationIssue();
            } catch (Exception e) {

//                if(onConnectionStatus!=null) onConnectionStatus.setNetworkStatus( DISCONNECTED );

                wifiNetworkStatusIntent.setAction("com.randmcnally.handsfreecalling.wifinetwork.status");
                wifiNetworkStatusIntent.putExtra("status", "Disconnected");

                context.sendBroadcast(wifiNetworkStatusIntent);


                e.printStackTrace();
            }
        }

    }


}

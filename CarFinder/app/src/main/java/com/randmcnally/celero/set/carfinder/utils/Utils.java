package com.randmcnally.celero.set.carfinder.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.randmcnally.celero.set.carfinder.R;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by mjunghare on 27/1/16.
 */
public class Utils {

    public static boolean isTablet = false;

    public static void setSharedPrefs(Context appContext, DataModel dataModel) {
        SharedPreferences sharedpreferences = appContext.getSharedPreferences(appContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.KEY_LATITUDE, dataModel.getLatitude());
        editor.putString(Constants.KEY_LONGITUDE, dataModel.getLongitude());
        editor.putString(Constants.KEY_STATUS, dataModel.getStatus());
        editor.putString(Constants.KEY_DEVICEID, dataModel.getDeviceId());
        editor.putString(Constants.KEY_DATE, dataModel.getDate());
        editor.putString(Constants.KEY_TIMESTAMP, dataModel.getTimestamp());
        editor.putString(Constants.KEY_PHOTO, dataModel.getPhoto());

        editor.commit();
    }

    public static boolean isLastLocExist(Context appContext) {
        String latitude = "";
        String longitude = "";
        SharedPreferences sharedpreferences = appContext.getSharedPreferences(appContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        latitude = sharedpreferences.getString(Constants.KEY_LATITUDE, "");
        longitude = sharedpreferences.getString(Constants.KEY_LONGITUDE, "");
        if (latitude.equalsIgnoreCase("") || longitude.equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }

    public static DataModel getSharedPrefs(Context appContext) {
        DataModel dataModel = new DataModel();
        SharedPreferences sharedpreferences = appContext.getSharedPreferences(appContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        dataModel.setLatitude(sharedpreferences.getString(Constants.KEY_LATITUDE, ""));
        dataModel.setLongitude(sharedpreferences.getString(Constants.KEY_LONGITUDE, ""));
        dataModel.setStatus(sharedpreferences.getString(Constants.KEY_STATUS, ""));
        dataModel.setDeviceId(sharedpreferences.getString(Constants.KEY_DEVICEID, ""));
        dataModel.setDate(sharedpreferences.getString(Constants.KEY_DATE, ""));
        dataModel.setTimestamp(sharedpreferences.getString(Constants.KEY_TIMESTAMP, ""));
        dataModel.setPhoto(sharedpreferences.getString(Constants.KEY_PHOTO, ""));
        return dataModel;
    }

    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date getUTCdatetimeAsDate(String date)
    {
        //note: doesn't check for null
        return stringDateToDate(date);
    }

    public static String getUTCdatetimeAsString()
    {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());
        return utcTime;
    }

    public static Date stringDateToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        Calendar cal1 = new GregorianCalendar();

        return dateToReturn;
    }

    //if date differs then it will return message like yesterday, 1 day before, etc.
    public static String getTimeDuration(String lastDate) {
        String timeDuration = "";
        String currentDate = getUTCdatetimeAsString();

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT);

        Date dLast = null;
        Date dCurrent = null;

        try {
            dLast = format.parse(lastDate);
            dCurrent = format.parse(currentDate);

            //in milliseconds
            long diff = dCurrent.getTime() - dLast.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");

            if(diffDays > 0) {
                if(diffDays == 1) {
                    timeDuration = "Yesterday";
                }else {
                    timeDuration = (int)diffDays+" days before";
                }
            }else {
                if(diffHours > 0) {
                    timeDuration = (int)diffHours+"h"+(int)diffMinutes+"m ago - Today";
                }else {
                    timeDuration = (int)diffMinutes+"m ago - Today";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeDuration;
    }

    public static String extractCurrentTimeFromDate(String date) {
        String time = "";
        Date dateObj = getUTCdatetimeAsDate(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateObj);
        int hour = cal.get(Calendar.HOUR);
        int min = cal.get(Calendar.MINUTE);

        time = hour+":"+min;
        if(hour < 12)
            time+="AM";
        else
            time+="PM";

        return time;
    }


    /*
    * This Function converts the String back to Bitmap
    * */
    public static Bitmap getBitmapFromString(String jsonString) {

        byte[] decodedString = Base64.decode(jsonString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    /*
    * This functions converts Bitmap picture to a string which can be
    * JSONified.
    * */
    public static String getStringFromBitmap(Bitmap bitmapPicture) {
        final int COMPRESSION_QUALITY = 100;
        String encodedImage = "";
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

}

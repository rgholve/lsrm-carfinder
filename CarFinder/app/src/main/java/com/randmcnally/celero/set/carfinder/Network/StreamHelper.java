package com.randmcnally.celero.set.carfinder.Network;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by vpatole on 9/15/15.
 */
public class StreamHelper {

    static final int BLOCK_SIZE = 64000;
    /**
     * The method writes down a line in a flow, without dependence from its length.
     * Standard methods allow to write only lines in length to 65535 byte.
     * The method breaks a line into substrings, writing down first of all substrings.
     * @param stream
     * @param str
     */
    public static void writeString (final DataOutputStream stream, final String str) throws IOException {
        final int len = str.length ();
        int number = len / BLOCK_SIZE;
        if (0 <len % BLOCK_SIZE) number ++;
        final String [] ar = new String [number];

        for (int i = 0; i <number; i ++)
        {
            final int beginIndex = i * BLOCK_SIZE;
            final int lastInd = beginIndex + BLOCK_SIZE;
            if (lastInd> len)
                ar [i] = str.substring (beginIndex, len);
            else
                ar [i] = str.substring (beginIndex, lastInd);
        }

        stream.writeInt(number);
        for (int i = 0; i <number; i ++)
            stream.writeUTF(ar [i]);

        stream.flush();
    }

    /**
    * Conjugate to WriteString a method. Reads the composite line written down in a flow.
    * @param stream
    * @return
            */
    public static String readString (final DataInputStream stream) throws IOException
    {
        final int len = stream.readInt ();
        final StringBuilder sb = new StringBuilder (len * BLOCK_SIZE);
        for (int i = 0; i < len; i++) {
            String temp = stream.readUTF();
            Log.e("abcd", "Read Packet :" + i + " : " + temp);
            sb.append(temp);
        }
        return sb.toString ();
    }
}

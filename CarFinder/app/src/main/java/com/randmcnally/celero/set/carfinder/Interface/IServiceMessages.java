package com.randmcnally.celero.set.carfinder.Interface;

/**
 * Created by mjunghare on 9/2/16.
 */
public interface IServiceMessages {
    void handleMessages(int state, String Message);
}

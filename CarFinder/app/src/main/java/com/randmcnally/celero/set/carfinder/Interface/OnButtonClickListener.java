package com.randmcnally.celero.set.carfinder.Interface;

/**
 * Created by mjunghare on 8/2/16.
 */
public interface OnButtonClickListener {

    void onClickPairButton();
    void onClickUnpairButton();

    void onBackPressed();
    void onClickParkCarButton();
}

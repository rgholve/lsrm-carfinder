package com.randmcnally.celero.set.carfinder.Interface;

/**
 * Created by mjunghare on 29/1/16.
 */
public interface IPairDeviceDialogClickListener {
    public void onSelectDevice(String address);
}

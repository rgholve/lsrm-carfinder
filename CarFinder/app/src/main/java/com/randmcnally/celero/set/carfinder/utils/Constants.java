package com.randmcnally.celero.set.carfinder.utils;

/**
 * Created by mjunghare on 27/1/16.
 */
public class Constants {

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 11;
    public static final int MESSAGE_READ = 21;
    public static final int MESSAGE_WRITE = 31;
    public static final int MESSAGE_DEVICE_NAME = 41;
    public static final int MESSAGE_TOAST = 51;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    public static final int REQUEST_ENABLE_BT = 32;
    public static final int REQUEST_CONNECT_DEVICE_SECURE = 33;
    public static final int REQUEST_CONNECT_DEVICE_INSECURE = 34;

    //Shared Pref's keys
    public static final String KEY_LATITUDE = "LATITUDE";
    public static final String KEY_LONGITUDE = "LONGITUDE";
    public static final String KEY_STATUS = "STATUS";
    public static final String KEY_DEVICEID = "DEVICEID";
    public static final String KEY_DATE = "DATE";
    public static final String KEY_TIMESTAMP = "TIMESTAMP";
    public static final String KEY_PHOTO = "PHOTO";

    public static final String MESSENGER = "MESSENGER";
    public static final int ADD_RESPONSE_HANDLER = 111;
}

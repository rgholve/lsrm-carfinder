package com.randmcnally.celero.set.carfinder.uiscreens;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.leapscale.btnetwork.BTConstants;
import com.example.leapscale.btnetwork.BluetoothNetwork;
import com.randmcnally.celero.set.carfinder.Interface.OnButtonClickListener;
import com.randmcnally.celero.set.carfinder.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeFragment extends Fragment {

    private OnButtonClickListener mClickListener;
    private Button btnPair;
    private Button btnPark;
    private ImageView imgPair;
    private ImageView imgPark;
    private ImageView imgSave;
    private TextView textviewPair;


    public static HomeFragment newInstance(OnButtonClickListener clickListener) {
        HomeFragment fragment = new HomeFragment();
        fragment.mClickListener = clickListener;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("HomeFragment","onCreateView");
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        imgPair = (ImageView) view.findViewById(R.id.imgPair);
        imgPark = (ImageView) view.findViewById(R.id.imgPark);
        imgSave = (ImageView) view.findViewById(R.id.imgSave);
        textviewPair = (TextView) view.findViewById(R.id.textview_pair);
        setListener();
    }

    private void setListener() {
        imgPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textviewPair.getText().toString().equalsIgnoreCase(getString(R.string.pair_btn_txt))) {
                    if (mClickListener != null)
                        mClickListener.onClickPairButton();
                } else if (textviewPair.getText().toString().equalsIgnoreCase(getString(R.string.unpair_btn_txt))) {
                    if (mClickListener != null)
                        mClickListener.onClickUnpairButton();
                }
            }
        });

        imgPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    manageFragment(ParkCarFragment.newInstance(mClickListener, true), ParkCarFragment.class.getName(), HomeFragment.class.getName());
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onClickParkCarButton();

            }
        });
    }

    public void manageFragment(Fragment fragment, String tag, String currentFragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        if (getFragmentManager().findFragmentByTag(currentFragment) != null) {
            fragmentTransaction.hide(getFragmentManager().findFragmentByTag(currentFragment));
        }
            fragmentTransaction.add(R.id.containerView, fragment, tag);

            fragmentTransaction.addToBackStack(currentFragment);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            int position = fragmentTransaction.commitAllowingStateLoss();
            Log.d("FRAGMENT_TAG", " manageFragment " + currentFragment + " Added to back stack  @ Position - " + position);
            manager.executePendingTransactions();
        }

    public void updatePairButtonText(String text) {
        if(textviewPair != null)
            textviewPair.setText(text);
    }


}

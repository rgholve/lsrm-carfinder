package com.randmcnally.celero.set.carfinder.utils;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.randmcnally.celero.set.carfinder.Interface.IPairDeviceDialogClickListener;
import com.randmcnally.celero.set.carfinder.R;

/**
 * Created by mjunghare on 29/1/16.
 */
public class BTPairDevicesDialog extends Dialog {


    Context mContext;
    IPairDeviceDialogClickListener mListener;

    ListView lvPairedDevices;
    ListView lvAvailableDevices;
    ProgressBar pbScan;
    TextView tvPairDevices;
    TextView tvScan;

    final String TAG = "BTPairDevicesDialog";
    private Button btnScan;

    BluetoothAdapter mBtAdapter;

    public BTPairDevicesDialog(Context context, IPairDeviceDialogClickListener listener) {
        super(context);
        this.mContext = context;
        this.mListener = listener;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = getLayoutInflater();
        View view = (View) inflater.inflate(R.layout.dialog_pairdevices, null);
        setContentView(view);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
//        init(view);
    }
/*
    private void init(View view) {
        lvPairedDevices = (ListView) view.findViewById(R.id.lvPairedDevices);
        lvAvailableDevices = (ListView) view.findViewById(R.id.lvAvailableDevices);
        tvPairDevices = (TextView) view.findViewById(R.id.tvPairDevices);
        tvScan = (TextView) view.findViewById(R.id.tvScan);
        pbScan = (ProgressBar) view.findViewById(R.id.pbScan);
        btnScan = (Button) view.findViewById(R.id.btnScan);
        setPairedDevicesAdapter();
        setAvailableDevicesAdapter();
        registerReceivers();
        setListners();
    }

    private void registerReceivers() {
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mContext.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        mContext.registerReceiver(mReceiver, filter);
    }

    public void setPairedDevicesAdapter() {
        ArrayList<String> arrayPairedDevices = new ArrayList<String>();
        arrayPairedDevices.add("Devicec 1");
        arrayPairedDevices.add("Devicec 2");
        DeviceAdapter pairedDevicesAdapter = new DeviceAdapter(mContext, arrayPairedDevices);
        lvPairedDevices.setAdapter(pairedDevicesAdapter);
        lvPairedDevices.setVisibility(View.VISIBLE);
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                Log.i("DEVICES", device.getName() + "\n" + device.getAddress());
                arrayPairedDevices.add(device.getName().toString() + "\n" + device.getAddress().toString());
            }
            Log.i("DEVICES size", ""+arrayPairedDevices.size());
            pairedDevicesAdapter.setArrayDevices(arrayPairedDevices);
        } else {
            String noDevices = "No paired Devices";
            arrayPairedDevices.add(noDevices);
            pairedDevicesAdapter.setArrayDevices(arrayPairedDevices);
        }
    }

    public void setAvailableDevicesAdapter() {
        availableDevicesAdapter = new DeviceAdapter(mContext, arrayAvailableDevices);
        lvPairedDevices.setAdapter(availableDevicesAdapter);
    }

    private void setListners() {
        lvPairedDevices.setOnItemClickListener(mDeviceClickListener);

        lvAvailableDevices.setOnItemClickListener(mDeviceClickListener);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doDiscovery();
            }
        });
    }

    */
/**
     * Start device discover with the BluetoothAdapter
     *//*

    private void doDiscovery() {
        Log.d(TAG, "doDiscovery()");

        // Indicate scanning in the title
        tvScan.setText("scanning");
        btnScan.setVisibility(View.GONE);
        lvAvailableDevices.setVisibility(View.VISIBLE);

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    ArrayList<String> arrayAvailableDevices = new ArrayList<String>();
    */
/**
     * The BroadcastReceiver that listens for discovered devices and changes the title when
     * discovery is finished
     *//*

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            arrayAvailableDevices = new ArrayList<String>();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    arrayAvailableDevices.add(device.getName() + "\n" + device.getAddress());
                }
                availableDevicesAdapter.setArrayDevices(arrayAvailableDevices);
                availableDevicesAdapter.notifyDataSetChanged();
                pbScan.setVisibility(View.GONE);
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (availableDevicesAdapter.getCount() == 0) {
                    String noDevices = "No devices found";
                    arrayAvailableDevices.add(noDevices);
                    availableDevicesAdapter.setArrayDevices(arrayAvailableDevices);
                    availableDevicesAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    */
/**
     * The on-click listener for all devices in the ListViews
     *//*

    private AdapterView.OnItemClickListener mDeviceClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect
            mBtAdapter.cancelDiscovery();

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            mListener.onSelectDevice(address);
            dismiss();
        }
    };
*/

}

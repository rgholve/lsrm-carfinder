package com.example.leapscale.btnetwork;

/**
 * Created by mjunghare on 8/2/16.
 */
public class BTConstants {

    //Bluetooth receiver
    public static final String BT_ACTION = "com.example.leapscale.btnetwork.action";

    //Bluetooth action key
    public static final String KEY_ACTION = "ACTION";

    public static final String KEY_MSG = "MSG";

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0011;       // we're doing nothing
    public static final int STATE_LISTEN = 0012;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 0013; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 0014;  // now connected to a remote device
    public static final int STATE_DISCONNECTED = 0015;  // now connected to a remote device
    public static final int MSG_RECEIVED = 0016;

}
